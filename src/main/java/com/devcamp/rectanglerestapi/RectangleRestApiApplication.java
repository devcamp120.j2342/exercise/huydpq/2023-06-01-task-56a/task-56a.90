package com.devcamp.rectanglerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RectangleRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RectangleRestApiApplication.class, args);
	}

}
